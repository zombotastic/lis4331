> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Alexander C. Boehm

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitucket 
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of Contacts app in Android Studio
* Screenshot of running Java Hello
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git branch - List, create, or delete branches

#### Assignment Screenshots:

*Screenshot of contacts app Android Studio:

![Contacts image one](img/contacts1.png)


![Contacts image two](img/contacts2.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/nexus5.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/zombotastic/bitbucketstationlocations/ "Bitbucket Station Locations")

*Bitbucket main repo*
[Bitbucket link main](https://bitbucket.org/zombotastic/lis4381/src/master/ "Bitbucket Main")