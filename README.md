> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4331 - Advanced Mobile Web Application Development

## Alexander C. Boehm 

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md")
    - install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions
    - Contacts app

2. [A2 README.md](a2/README.md "My A2 README.md")
    - Backwards-engineer app My Tip Calculator
    - Screenshots of the program
    - Skill sets one, two, and three 

3. [A3 README.md](a3/README.md "My A3 README.md")
    - Create the App Currency Converter
    - Create one terminal program and two GUI programs
  
4. [P1 README.md](p1/README.md "My P1 README.md")
    - Include Music Player app which implements MediaPlayer and onClickListener
    - Show the apps functionality
    - Skill sets seven, eight, and nine

  
5. [A4 README.md](a4/README.md "My A4 README.md")
    - Create an app which implement preferred preferrences and persitent data
    - Includes Skill Sets 10, 11, & 12
    
6. [A5 README.md](a5/README.md "My A5 README.md")
    - Create an application which loads, formats, and displays an RSS Feed
    - Skill sets thirteen, fourteen, and fifteen

7. [P2 README.md](p2/README.md "My P2 README.md")
    - One app which implements sqlite to store client information

