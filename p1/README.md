# LIS4331 - Advanced Mobile Application Development

## Alexander C. Boehm

### Project 1 Requirements:
1. Include Music Player app which implements MediaPlayer and onClickListener
2. Show the apps functionality
3. Skill sets seven, eight, and nine

#### Animated GIF of My Event Application Running ADS:
![SlickDubs](https://media.giphy.com/media/uN0XzNEX9yjTtBTq3A/giphy.gif "SlickDubs")

#### Skill Sets Seven, & Eight

|Skill Set Seven | Skill Set Eight |
---|---
|![SS7 VS Code](img/ss7.png "SS7 VS Code")|![SS8 VS Code](img/ss8.png "SS8 VS Code")|

#### Skill Set Nine
|![SS9 VS code](https://media.giphy.com/media/s1dpzxD0hMkPX39lYI/giphy.gif "SS9")|

#### Video
[![slickdubs](img/youtubefake.png)](https://www.youtube.com/watch?v=JarCFVSrvf8&feature=youtu.be&ab_channel=Alexpoopslie)




