# LIS4331 - Mobile Web Application Development

## Alexander C. Boehm

### Assignment 4 Requirements:

*Three Parts:*

1. Create an app which implement preferred preferences and persistent data
2. Skill sets ten, eleven, and twelve

#### Screenshots of My Event Application Running ADS:

### App Running ADS
|![Interest Calculator](https://media.giphy.com/media/rE1fWS7esyC3qjSZdP/giphy.gif "My Portfolio")|

#### Skill Sets Ten, Eleven, & Twelve

|Skill Set Ten | Skill Set Eleven |
---|---
|![SS10 VS Code](img/ss10.png "SS10 VS Code")|![SS11 VS Code](img/ss11.png "SS11 VS Code")|![SS12 VS Code]|

#### Skill set Twelve
![SS12](img/ss12.png) "SS12"