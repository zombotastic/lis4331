# LIS4331 - Advanced Mobile Application Development

## Alexander C. Boehm

### Assignment 3 Requirements:

*Two Parts:*

1. Create the App Currency Converter
2. Create one terminal program and two GUI programs 

#### Animated GIF of Currency Converter Running ADS

![My GIF](https://media.giphy.com/media/TalXfVv1z5d0e77AO7/giphy.gif "My GIF")|

#### Skill Set Four

![SS4 VS Code](img/ss4.png "SS4 VS Code")

#### Skill Set Five

![part1](img/ss5/part1.png "ss5 part1")
![part2](img/ss5/part2.png "ss5 part2")
![part3](img/ss5/part3.png "ye") 


#### Skill Set Six

![pt1](img/ss6/part1.png "pt1")
![pt2](img/ss6/part2.png "pt2")
![pt3](img/ss6/part3.png "pt3")
![pt4](img/ss6/part4.png "pt4")
![pt5](img/ss6/part5.png "pt5")
![pt6](img/ss6/part6.png "pt6")
![pt7](img/ss6/part7.png "pt7")
![pt8](img/ss6/part8.png "pt8")
