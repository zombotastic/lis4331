# LIS4331 - Mobile Web Application Development

## Alexander C. Boehm

### Assignment 2 Requirements:

*three Parts:*

1. Backwards-engineer an app which includes multiple views and completes some calculation implementing an OnClickListener
2. Screenshots of the program
3. Skill sets one, two, and three

#### Screenshots of My Tip Calculator! application running ADS:

| Part 1 | Part 2|
--- | ---
|![My Tip A](img/tip1.png "My Tip A")|![My Events B](img/tip2.png "My Tip B")|

#### Skill sets One, Two, & Three

|Skill Set One | Skill Set Two | Skill Set Three |
---|---|---
|![SS1 VS Code](img/ss1.png "SS1 VS Code")|![SS2 VS Code](img/ss2.png "SS2 VS Code")|![SS3 VS Code](img/ss3.png "SS3 VS Code")|

