# LIS4331 - Advanced Mobile Web Application Development

## Alexander C. Boehm

### Assignment 5 Requirements:

*Two Parts:*

1. Create an application which loads, formats, and displays an RSS Feed
2. Skill sets thirteen, fourteen, and fifteen

#### GIF of Assignnment 5:

![GIF of a5](https://media.giphy.com/media/4p8cAabA4S8yPpUZqO/giphy.gif "a5 GIF")

#### Skill Set Thirteen

![SS13 VS Code](img/ss13.png "SS10 VS Code")

#### Skill Set Forteen
![SS14 VS Code](img/ss14.png "SS14 VS Code")

#### Skill Set Fifteen
![SS14 VS Code](img/ss15.png "SS14 VS Code")